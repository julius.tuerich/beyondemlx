import {expect, test} from 'vitest'
import {api} from "../../src/typescript/api";

const {exec} = require("child_process")

test('integration of frontend and backend', async () => {
    const directory = __dirname + "/../../.."
    const serverScript = "cli/integration-test.py"
    const configFile = "config/run.yaml"
    const command = `cd ${directory} && python3 ${serverScript} ${configFile}`
    exec(command, (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log(`stdout: ${stdout}`);
    });

    // sleep for 5 seconds
    await new Promise(resolve => setTimeout(resolve, 5000));

    // test the API
    const cypherTest = "MATCH (n) RETURN n LIMIT 1"
    const result = api.query(cypherTest)

    expect(result).toBeInstanceOf(Object)
})