from beyondemlx.emlx.parts.Participant import Participant
from beyondemlx.events.Embedding.EmbeddingEvent import EmbeddingEvent


class EmbeddingNewUserDomainRelationEvent(EmbeddingEvent):

    def __init__(self, user: Participant, domain: str, relation: str):
        self.user = user
        self.domain = domain
        self.relation = relation

    def get_user(self) -> Participant:
        """
        Returns the user.
        """
        return self.user

    def get_domain(self) -> str:
        """
        Returns the domain.
        """
        return self.domain

    def get_relation(self) -> str:
        """
        Returns the relation.
        """
        return self.relation