from abc import ABC


class Service(ABC):

    def register_events(self):
        """
        Registers the event to listen to.
        """
        pass

    def start(self):
        """
        Starts the service.
        """
        pass