from beyondemlx.emlx.EmlxMail import EmlxMail
from beyondemlx.events.Embedding.EmbeddingEvent import EmbeddingEvent


class EmbeddingNewMailEvent(EmbeddingEvent):

    def __init__(self, mail: EmlxMail):
        self.mail = mail

    def get_mail(self) -> EmlxMail:
        """
        Returns the mail.
        """
        return self.mail