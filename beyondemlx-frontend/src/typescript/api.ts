import axios from "axios";

class API {
    _getHost() {
        return import.meta.env.VITE_API_HOST;
    }

    async query(query: string) {
        const result = await axios.post(`${this._getHost()}/api/query`, {
            query: query
        })

        const data = result.data;

        if (data.meta.code != 200) {
            throw new Error(data.meta.message);
        }
        return result.data.data;
    }

    async openMail(path: string) {
        const result = await axios.post(`${this._getHost()}/api/open-mail`, {
            path: path,
        })

        const data = result.data;

        if (data.meta.code != 200) {
            throw new Error(data.meta.message);
        }
        return result.data;
    }

    async getMailDirectory(): Promise<string> {
        const result = await axios.post(`${this._getHost()}/api/mail-directory`, {})

        const data = result.data;

        if (data.meta.code != 200) {
            throw new Error(data.meta.message);
        }
        return data.data.path as string;
    }
}

export const api = new API();