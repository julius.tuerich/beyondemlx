import re

from beyondemlx.anonymizer.steps.Step import Step
from beyondemlx.emlx.EmlxMail import EmlxMail


class CopyAndAnonymizeSubject(Step):

    def execute(self, mail: EmlxMail, anonymized_mail: EmlxMail) -> EmlxMail:
        """
        Replace the subject of the mail with a new subject.
        """
        subject = mail.get_subject()
        subject = self.faker.subject(subject)
        anonymized_mail.add_header('Subject', subject)

        return mail
