import re

from beyondemlx.anonymizer.steps.Step import Step
from beyondemlx.emlx.EmlxMail import EmlxMail


class CopyAndAnonymizeBody(Step):

    def execute(self, mail: EmlxMail, anonymized_mail: EmlxMail) -> EmlxMail:
        """
        Replace the body of the mail with a new body.
        """
        body = mail.get_body()
        # strip tags from body
        new_body = re.sub('<[^<]+?>', '', body)
        new_body = str(len(new_body))
        anonymized_mail.set_body(new_body)

        return mail
