from beyondemlx.events.Embedding.EmbeddingEvent import EmbeddingEvent


class EmbeddingNewDomainEvent(EmbeddingEvent):

    def __init__(self, domain: str):
        self.domain = domain

    def get_domain(self) -> str:
        """
        Returns the domain.
        """
        return self.domain.lower()