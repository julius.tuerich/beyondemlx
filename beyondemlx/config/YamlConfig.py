from abc import ABC

import yaml


class YamlConfig(ABC):
    def __init__(self, config_file_path: str):
        self.config_file_path = config_file_path
        self.config = None

    def load(self):
        """
        Loads the config from the yaml file.
        """
        with open(self.config_file_path, 'r') as stream:
            try:
                self.config = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)

        # check if config is valid
        if not self.is_valid():
            raise Exception("Config is not valid!")

    def is_valid(self):
        """
        Checks if the config is valid
        """
        return True

    def has(self, key: str):
        """
        Returns true if the config has the given key.
        """
        return key in self.config

    def get(self, key: str):
        """
        Returns the value for the given key.
        """
        return self.config[key]

    def set(self, key: str, value: object):
        """
        Sets the value for the given key.
        """
        self.config[key] = value

    def save(self):
        """
        Saves the config to the yaml file.
        """
        with open(self.config_file_path, 'w') as stream:
            yaml.dump(self.config, stream, default_flow_style=False)

    def __str__(self):
        """
        Returns a string representation of this object.
        """
        return "YamlConfig[config_file_path={}]".format(self.config_file_path)