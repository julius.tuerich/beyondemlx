# Integration Tests

The integration tests of the two components, the `frontend` and the `backend`, are located in the `/beyondemlx-frontend/tests/integration` directory. To run these tests, ensure you followed the instructions from the `/README.md` file in the root directory of the project. A `/config/run.yaml` file is required to run the tests. Also the Neo4j database must be running.