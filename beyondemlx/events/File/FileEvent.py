from beyondemlx.events.Event import Event


class FileEvent(Event):

    def __init__(self, src_path: str):
        self.src_path = src_path

    def get_src_path(self) -> str:
        """
        Returns the source path.
        """
        return self.src_path