import logging

from neo4j import GraphDatabase
from pydispatch import dispatcher
from dateutil import parser

from beyondemlx.config.YamlRunConfig import YamlRunConfig
from beyondemlx.emlx.EmlxMail import EmlxMail
from beyondemlx.events import Events
from beyondemlx.events.Database.DatabaseDeletedRecordEvent import DatabaseDeletedRecordEvent
from beyondemlx.events.Database.DatabaseNewRecordEvent import DatabaseNewRecordEvent
from beyondemlx.events.Embedding import EmbeddingNewUserDomainRelationEvent
from beyondemlx.events.Embedding.EmbeddingNewDomainEvent import EmbeddingNewDomainEvent
from beyondemlx.events.Embedding.EmbeddingNewMailEvent import EmbeddingNewMailEvent
from beyondemlx.events.Embedding.EmbeddingNewUserEmailRelationEvent import EmbeddingNewUserEmailRelationEvent
from beyondemlx.events.Embedding.EmbeddingNewUserEvent import EmbeddingNewUserEvent
from beyondemlx.events.File.FileDeletedEvent import FileDeletedEvent
from beyondemlx.services.Service import Service


class DatabaseService(Service):

    def __init__(self, config: YamlRunConfig):
        self.config = config
        self.driver = None
        self.session = None

    def register_events(self):
        """
        Registers the event to listen to.
        """
        dispatcher.connect(self.handle_system_start, signal=Events.EVENT_SYSTEM_START)
        dispatcher.connect(self.handle_system_kill, signal=Events.EVENT_SYSTEM_KILL)
        dispatcher.connect(self.handle_system_db_wipe, signal=Events.EVENT_SYSTEM_DB_WIPE)
        dispatcher.connect(self.handle_new_domain, signal=Events.EVENT_EMBEDDING_NEW_DOMAIN)
        dispatcher.connect(self.handle_new_user, signal=Events.EVENT_EMBEDDING_NEW_USER)
        dispatcher.connect(self.handle_new_mail, signal=Events.EVENT_EMBEDDING_NEW_MAIL)
        dispatcher.connect(self.handle_new_user_domain_relation, signal=Events.EVENT_EMBEDDING_NEW_USER_DOMAIN_RELATION)
        dispatcher.connect(self.handle_new_user_email_relation, signal=Events.EVENT_EMBEDDING_NEW_USER_EMAIL_RELATION)
        dispatcher.connect(self.handle_deleted_emlx_mail, signal=Events.EVENT_FILE_DELETED_EMLX_MAIL)

    def handle_system_start(self):
        """
        Handles the system start.
        """
        logging.info("DatabaseService.handle_system_start")

        # connect to database
        self.connect()

        # create indexes on Domain.name, User.name, User.email, Mail.subject, Mail.date, Mail.message_id
        session = self.get_session()
        session.run("CREATE INDEX domain_name IF NOT EXISTS FOR (d:Domain) ON (d.name)")
        session.run("CREATE INDEX user_name IF NOT EXISTS FOR (u:User) ON (u.name)")
        session.run("CREATE INDEX user_email IF NOT EXISTS FOR (u:User) ON (u.email)")
        session.run("CREATE INDEX mail_subject IF NOT EXISTS FOR (m:Mail) ON (m.subject)")
        session.run("CREATE INDEX mail_date IF NOT EXISTS FOR (m:Mail) ON (m.date)")
        session.run("CREATE INDEX mail_message_id IF NOT EXISTS FOR (m:Mail) ON (m.message_id)")
        session.run("CREATE INDEX mail_replying_to_message_id IF NOT EXISTS FOR (m:Mail) ON (m.replying_to_message_id)")

    def handle_system_kill(self):
        """
        Handles the system kill.
        """
        logging.info("DatabaseService.handle_system_kill")

        # disconnect from database
        self.disconnect()

    def handle_system_db_wipe(self):
        """
        Handles the system db wipe.
        """
        logging.info("DatabaseService.handle_system_db_wipe")

        # wipe database
        session = self.get_session()
        session.run("MATCH (n) DETACH DELETE n")

    def handle_new_domain(self, event: EmbeddingNewDomainEvent):
        """
        Handles a new embedding.
        """
        logging.info("DatabaseService.handle_new_domain")
        session = self.get_session()
        session.run("MERGE (d:Domain {name: $name})", name=event.get_domain())

    def handle_new_user(self, event: EmbeddingNewUserEvent):
        """
        Handles a new embedding.
        """
        logging.info("DatabaseService.handle_new_user")
        session = self.get_session()
        user = event.get_user()
        session.run("MERGE (u:User {name: $name, email: $email})",
                    name=user.get_name_or_guessed_name(),
                    email=user.get_email())

    def handle_new_mail(self, event: EmbeddingNewMailEvent):
        """
        Handles a new embedding.
        """
        logging.info("DatabaseService.handle_new_mail")
        session = self.get_session()
        mail = event.get_mail()

        date = mail.get_header('Date')
        replying_to_message_id = mail.get_replying_to_message_id()
        if date is None:
            date = 'unknown'
        else:
            # parse date
            date = parser.parse(date)
        if replying_to_message_id is None:
            replying_to_message_id = 'NONE'


        session.run("MERGE (m:Mail {message_id: $message_id, file_name: $file_name, subject: $subject, date: $date, attachments: $attachments, replying_to_message_id: $replying_to_message_id})",
                    message_id=mail.get_message_id(),
                    file_name=mail.get_path(),
                    subject=mail.get_subject(),
                    date=date,
                    attachments=len(mail.get_attachments()),
                    replying_to_message_id=replying_to_message_id)

    def handle_new_user_domain_relation(self, event: EmbeddingNewUserDomainRelationEvent):
        """
        Handles a new user domain relation.
        """
        logging.info("DatabaseService.handle_new_relation")
        session = self.get_session()
        session.run("MATCH (u:User {email: $email}), (d:Domain {name: $domain}) MERGE (u)-[r:" + event.get_relation() + "]->(d)",
                    email=event.get_user().get_email(),
                    domain=event.get_domain())

    def handle_new_user_email_relation(self, event: EmbeddingNewUserEmailRelationEvent):
        """
        Handles a new user email relation.
        """
        logging.info("DatabaseService.handle_new_relation")
        session = self.get_session()
        session.run("MATCH (u:User {email: $email}), (m:Mail {message_id: $message_id}) MERGE (u)<-[r:" + event.get_relation() + "]-(m)",
                    email=event.get_user().get_email(),
                    message_id=event.get_email().get_message_id())

    def handle_deleted_emlx_mail(self, event: FileDeletedEvent):
        """
        Handles a deleted emlx mail.
        """
        logging.info("DatabaseService.handle_deleted_emlx_mail")
        session = self.get_session()
        session.run("MATCH (m:Mail {file_name: $file_name}) DETACH DELETE m",
                    file_name=event.get_src_path())
        self.dispatch_deleted_record_event(None)

    def dispatch_new_record_event(self, record: object):
        """
        Dispatches a new record event.
        """
        dispatcher.send(Events.EVENT_DATABASE_NEW_RECORD, sender=self, event=DatabaseNewRecordEvent())

    def dispatch_deleted_record_event(self, record: object):
        """
        Dispatches a deleted record event.
        """
        dispatcher.send(Events.EVENT_DATABASE_DELETED_RECORD, sender=self, event=DatabaseDeletedRecordEvent())

    def connect(self):
        """
        Connects to the database.
        """
        self.driver = GraphDatabase.driver(self.config.get('NEO4J_URI'),
                                           auth=(self.config.get('NEO4J_USER'), self.config.get('NEO4J_PASSWORD')))

    def disconnect(self):
        """
        Disconnects from the database.
        """
        self.driver.close()

    def get_session(self):
        """
        Gets a session.
        """
        if self.session is None:
            self.session = self.driver.session()
        return self.session

    def execute_query(self, query: str):
        """
        Executes a query.
        """
        session = self.get_session()
        result = session.run(query)
        return result.graph()
