from beyondemlx.anonymizer.Faker import Faker


def test_faker_name():
    faker = Faker()
    assert faker.first_name('Julius') == 'Ryan'
    assert faker.last_name('Türich') == 'Miller'
    assert faker.name('Julius Türich') == 'Brenda Leonard'


def test_faker_email():
    faker = Faker()
    assert faker.email('julius.tuerich@uni-bielefeld.de') == 'tammy43@smith.net'
    assert faker.email('thomas.hermann@uni-bielefeld.de') == 'anthony59@smith.net'
    assert faker.email('j.tuerich@techfak.de') == 'bentonrobert@gomez.com'


def test_faker_ipv4():
    faker = Faker()
    assert faker.ipv4('108.92.264.156') == '199.143.232.54'


def test_faker_email_contents():
    faker = Faker()
    assert faker.sentence('Hello world!') == 'Instead by skill bed through.'
    assert faker.subject('Re: Hello world!') == 'Re: Town simple consumer everybody officer myself.'
    assert faker.subject('Re: FWD: RE Hello world!') == 'Re: FWD: RE Town simple consumer everybody officer myself.'
    assert faker.body('Hello World.') == 'According u.'
    assert faker.body('Lorem ipsum dolor sit amet.') == 'Mother indeed accept degre.'
