import os
import sys
import uuid


sys.path.append('../beyondemlx')

from beyondemlx.anonymizer.steps.CopyDate import CopyDate
from beyondemlx.anonymizer.steps.CopyAndAnonymizeBody import CopyAndAnonymizeBody
from beyondemlx.anonymizer.steps.CopyAndAnonymizeFrom import CopyAndAnonymizeFrom
from beyondemlx.anonymizer.steps.CopyAndAnonymizeSubject import CopyAndAnonymizeSubject
from beyondemlx.anonymizer.steps.CopyAndAnonymizeMessageIds import CopyAndAnonymizeMessageIds
from beyondemlx.anonymizer.steps.CopyAndAnonymizeTo import CopyAndAnonymizeTo
from beyondemlx.anonymizer.steps.CopyAndAnonymizeBCC import CopyAndAnonymizeBCC
from beyondemlx.anonymizer.steps.CopyAndAnonymizeCC import CopyAndAnonymizeCC

from beyondemlx.anonymizer.Anonymizer import Anonymizer
from beyondemlx.anonymizer.Pipeline import Pipeline
from beyondemlx.emlx.EmlxMail import EmlxMail


def copy_files_recursively(input_folder, output_folder):
    """
    Copy files recursively from input folder to output folder.
    """
    # get all files and folders in input folder
    files_and_folders = os.listdir(input_folder)

    # iterate through all files and folders
    for file_or_folder in files_and_folders:
        # get full path of file or folder
        full_path = os.path.join(input_folder, file_or_folder)

        # check if file or folder
        if os.path.isfile(full_path):
            try:
                print('Copying file: ' + full_path)
                # copy file
                copy_file(full_path, output_folder)
            except Exception as e:
                print('Error while copying file: ' + full_path)
                print(e)
        else:
            if file_or_folder == "Attachments":
                continue

            # hash folder name if folder is not numeric
            if not file_or_folder.isnumeric() and not file_or_folder == "Messages":
                file_or_folder = str(uuid.uuid4())

            print('Copying folder: ' + full_path)
            # create folder in output folder
            new_output_folder = os.path.join(output_folder, file_or_folder)
            os.mkdir(new_output_folder)

            # copy files recursively
            copy_files_recursively(full_path, new_output_folder)


def copy_file(input_file, output_folder):
    """
    Copy file from input file to output folder.
    """
    # get file name
    file_name = os.path.basename(input_file)

    # only copy emlx files
    if not file_name.endswith(".emlx"):
        return

    # create output file name
    mail = EmlxMail.from_file(input_file)

    if not mail.has_necessary_headers():
        return

    # anonymize mail
    anonymizer = get_anonymizer()
    anonymized_mail = anonymizer.anonymize_mail(mail)

    # write mail to disk
    output_file = os.path.join(output_folder, file_name)
    anonymized_mail.path = output_file
    anonymized_mail.write_to_disk(output_file)

    # copy attachments
    if mail.has_attachments():
        # create attachment folder if not exists
        attachment_folder = anonymized_mail.get_attachment_folder()
        attachment_base_folder = anonymized_mail.get_attachment_base_folder()

        if not os.path.isdir(attachment_base_folder):
            os.mkdir(attachment_base_folder)

        if not os.path.isdir(attachment_folder):
            os.mkdir(attachment_folder)

        # copy attachments
        for attachment in mail.get_attachments():
            # create empty file with same name
            # hash attachment name but keep extension
            attachment_name = str(uuid.uuid4()) + os.path.splitext(attachment.get_name())[1]
            attachment_path = os.path.join(attachment_folder, attachment_name)
            open(attachment_path, 'a').close()

def get_anonymizer() -> Anonymizer:
    """
    Get anonymizer.
    """
    pipeline = Pipeline()
    pipeline.add(CopyAndAnonymizeBody())
    pipeline.add(CopyAndAnonymizeFrom())
    pipeline.add(CopyAndAnonymizeTo())
    pipeline.add(CopyAndAnonymizeCC())
    pipeline.add(CopyAndAnonymizeBCC())
    pipeline.add(CopyAndAnonymizeSubject())
    pipeline.add(CopyAndAnonymizeMessageIds())
    pipeline.add(CopyDate())

    return Anonymizer(pipeline)

if __name__ == "__main__":

    # m = EmlxMail.from_file("/Users/julius/Desktop/93F89807-ACC2-4442-B91A-4A2E419C7D63/Posteingang.mbox/6 - Archiv.mbox/F09806D4-95ED-407E-92C7-ADDB04827D37/Data/0/8/Messages/80057.partial.emlx")
    # print(m.has_attachments())
    # exit()

    # get input folder
    input_folder = sys.argv[1]

    # get output folder
    output_folder = sys.argv[2]

    # assert that input folder exists
    if not os.path.exists(input_folder):
        print("Input folder does not exist")
        sys.exit(1)

    # assert that input folder is a directory
    if not os.path.isdir(input_folder):
        print("Input folder is not a directory")
        sys.exit(1)

    # create output folder if it does not exist
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)

    # assert that output folder is a directory
    if not os.path.isdir(output_folder):
        print("Output folder is not a directory")
        sys.exit(1)

    # create random folder name
    random_folder_name = str(uuid.uuid4())

    # create temporary folder
    temp_folder = os.path.join(output_folder, random_folder_name)
    os.mkdir(temp_folder)

    # assert that output folder is not a subdirectory of input folder
    if input_folder in output_folder:
        print("Output folder is a subdirectory of input folder")
        sys.exit(1)

    # copy files recursively
    copy_files_recursively(input_folder, temp_folder)

    # print success message
    print("Successfully copied files from {} to {}".format(input_folder, temp_folder))
