from beyondemlx.emlx.parts.Participant import Participant
from beyondemlx.events.Embedding.EmbeddingEvent import EmbeddingEvent


class EmbeddingNewUserEvent(EmbeddingEvent):

    def __init__(self, user: Participant):
        self.user = user

    def get_user(self) -> Participant:
        """
        Returns the user.
        """
        return self.user