import os.path

from beyondemlx.anonymizer.Anonymizer import Anonymizer
from beyondemlx.emlx.EmlxMail import EmlxMail


def get_mail():
    return EmlxMail.from_file(
        os.path.dirname(__file__) + '/data/80252.emlx')


def test_body_length():
    mail = get_mail()
    assert mail.get_body_length() == 17967


def test_from():
    mail = get_mail()
    sender = mail.get_from()

    assert len(sender) == 1
    assert sender[0].get_name() == 'Loom Team'
    assert sender[0].get_email() == 'no-reply@loom.com'

def test_to():
    mail = get_mail()
    receiver = mail.get_to()

    assert len(receiver) == 1
    assert receiver[0].get_name() == 'office@uni-bielefeld.de'
    assert receiver[0].get_email() == 'office@uni-bielefeld.de'

def test_subject():
    mail = get_mail()
    subject = mail.get_subject()
    assert subject == 'Only 3 days left on your free trial of Loom Business'

def test_attachments():
    assert len(get_mail().get_attachments()) == 0
    assert get_mail().has_attachments() == False
