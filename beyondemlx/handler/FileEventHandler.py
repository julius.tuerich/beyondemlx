from watchdog.events import FileSystemEventHandler


class FileEventHandler(FileSystemEventHandler):

    def __init__(self, callback_on_created=None, callback_on_deleted=None, callback_on_moved=None):
        super().__init__()

        self.callback_on_created = callback_on_created
        self.callback_on_deleted = callback_on_deleted
        self.callback_on_moved = callback_on_moved

    def on_created(self, event):
        """
        Callback for when a file is created.
        """
        if self.callback_on_created:
            self.callback_on_created(event)


    def on_deleted(self, event):
        """
        Callback for when a file is deleted.
        """
        if self.callback_on_deleted:
            self.callback_on_deleted(event)


    def on_moved(self, event):
        """
        Callback for when a file is moved.
        """
        if self.callback_on_moved:
            self.callback_on_moved(event)