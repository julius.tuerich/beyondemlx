from beyondemlx.anonymizer.Pipeline import Pipeline
from beyondemlx.emlx.EmlxMail import EmlxMail


class Anonymizer(object):
    def __init__(self, pipeline: Pipeline):
        """
        Initialize the anonymizer.
        """
        self.pipeline = pipeline

    def anonymize_mail(self, mail: EmlxMail) -> EmlxMail:
        """
        Anonymize a mail and write the result to another file.
        """
        anonymized_mail = EmlxMail()

        # execute pipeline
        self.pipeline.execute(mail, anonymized_mail)

        return anonymized_mail



