import base64

from beyondemlx.emlx.unicode_utils import decode_rfc_1342


def test_anonymizer():
    subject = "=?utf-8?B?SGVsbG8gV29ybGQ=?="

    assert decode_rfc_1342(subject) == "Hello World"