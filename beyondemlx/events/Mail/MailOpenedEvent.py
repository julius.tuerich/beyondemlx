from beyondemlx.events.Mail.MailEvent import MailEvent


class MailOpenedEvent(MailEvent):

    def __init__(self, path: str):
        self.path = path
        pass