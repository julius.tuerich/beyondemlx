import os

from beyondemlx.anonymizer.Anonymizer import Anonymizer
from beyondemlx.anonymizer.Pipeline import Pipeline
from beyondemlx.anonymizer.steps.CopyAndAnonymizeBCC import CopyAndAnonymizeBCC
from beyondemlx.anonymizer.steps.CopyAndAnonymizeBody import CopyAndAnonymizeBody
from beyondemlx.anonymizer.steps.CopyAndAnonymizeCC import CopyAndAnonymizeCC
from beyondemlx.anonymizer.steps.CopyAndAnonymizeFrom import CopyAndAnonymizeFrom
from beyondemlx.anonymizer.steps.CopyAndAnonymizeMessageIds import CopyAndAnonymizeMessageIds
from beyondemlx.anonymizer.steps.CopyAndAnonymizeSubject import CopyAndAnonymizeSubject
from beyondemlx.anonymizer.steps.CopyAndAnonymizeTo import CopyAndAnonymizeTo
from beyondemlx.anonymizer.steps.CopyDate import CopyDate
from beyondemlx.emlx.EmlxMail import EmlxMail


def get_mail():
    return EmlxMail.from_file(
        os.path.dirname(__file__) + '/data/80252.emlx')

def get_anonymized_mail_content():
    return """
From: Lawrence Lopez <jonathan14@boone.info>
To: Michael Johnson <wcampbell@smith.net>
Subject: Plant enjoy argue chance.
Message-Id: af69ac51-e74a-4229-a609-3b2fdc97b748@johnson.com
Date: Mon, 25 Sep 2023 10:27:49 +0000"""

def test_anonymizer():
    mail = get_mail()

    pipeline = Pipeline()
    pipeline.add(CopyAndAnonymizeBody())
    pipeline.add(CopyAndAnonymizeFrom())
    pipeline.add(CopyAndAnonymizeTo())
    pipeline.add(CopyAndAnonymizeCC())
    pipeline.add(CopyAndAnonymizeBCC())
    pipeline.add(CopyAndAnonymizeSubject())
    pipeline.add(CopyAndAnonymizeMessageIds())
    pipeline.add(CopyDate())

    anonymizer = Anonymizer(pipeline)
    anonymized_mail = anonymizer.anonymize_mail(mail)
    anonymized_mail.write_to_disk(os.path.dirname(__file__) + '/tmp/80252.emlx')

    assert get_anonymized_mail_content() in anonymized_mail.get_content()