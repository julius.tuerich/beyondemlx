from beyondemlx.emlx.unicode_utils import decode_rfc_1342


class Participant(object):
    def __init__(self, name, email):
        """
        Create a participant with a name and an email.
        """
        self.name = name
        self.email = email

    def get_name(self) -> str:
        """
        Get the name of the participant.
        """
        name = decode_rfc_1342(self.name)

        prefixes = ["Cc", "Bcc", "From", "To"]
        for prefix in prefixes:
            name = (name
                    .replace(prefix + ": ", "")
                    .replace(prefix.lower() + ": ", "")
                    .replace(prefix.upper() + ": ", ""))
        return name

    def get_email(self) -> str:
        """
        Get the email of the participant.
        """
        return self.email.lower()

    def get_domain(self) -> str:
        """
        Get the domain of the participant.
        """
        return self.email.split('@')[1]

    def get_name_or_guessed_name(self):
        """
        Get the name of the participant or guess it from the email.
        """
        name = self.get_name()

        if name == '':
            name = self.email.split('@')[0] + ' (' + self.get_domain() + ')'

        # make name lowercase
        name = name.lower()

        # make first letters in words uppercase
        name = name.title()

        return name

    @staticmethod
    def from_string(string) -> 'Participant':
        """
        Create a participant from a string.
        """
        if '<' not in string:
            return Participant(string, string)

        # if multiple < in string cut off everything after the second <
        if string.count('<') > 1:
            string = string.split('<')[0] + '<' + string.split('<')[1]

        name, email = string.split('<')
        name = name.strip()
        email = email[:-1]

        # trim name if present
        if name.startswith(' '):
            name = name[1:]
        if name.endswith(' '):
            name = name[:-1]

        # remove quotes from name if present
        if name.startswith('"') and name.endswith('"'):
            name = name[1:-1]

        return Participant(name, email)

    def to_dict(self) -> dict:
        """
        Convert the participant to a dictionary.
        """
        return {
            'name': self.name,
            'email': self.email,
            'domain': self.get_domain(),
        }
