import base64, quopri


def decode_rfc_1342(string) -> str:
    """
    Decode RFC 1342 encoded strings
    """
    words = string.split(" ")
    for i in range(len(words)):
        word = words[i]
        if "=?utf-8?" in word.lower() or "=?iso-8859" in word.lower():
            try:
                word = word[2:-2]
                encoding, enc_type, word = word.split("?", 2)

                if enc_type.upper() == "B":
                    word = base64.decodebytes(bytes(word, 'utf-8')).decode(encoding.lower())
                elif enc_type.upper() == "Q":
                    word = quopri.decodestring(word).decode(encoding.lower())

                word = word.replace(",", "")
                word = word.replace("_", " ")
                words[i] = word
            except ValueError:
                words[i] = word.replace(",", "")

    line = " ".join(words)

    return line
