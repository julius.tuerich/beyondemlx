import datetime

from dateutil.tz import tzutc

from beyondemlx.emlx.EmlxMail import EmlxMail
from beyondemlx.emlx.parts.Participant import Participant
from beyondemlx.events.Embedding.EmbeddingNewDomainEvent import EmbeddingNewDomainEvent
from beyondemlx.events.Embedding.EmbeddingNewMailEvent import EmbeddingNewMailEvent
from beyondemlx.events.Embedding.EmbeddingNewUserEvent import EmbeddingNewUserEvent
from beyondemlx.services.DatabaseService import DatabaseService
from beyondemlx.config.YamlConfig import YamlConfig
import pathlib


class MockDriver:
    def __init__(self):
        self.driver_session = MockSession()
        pass

    def session(self):
        return self.driver_session


class MockSession:
    def __init__(self):
        self.query = None
        self.kwargs = None

    def run(self, query, **kwargs):
        self.query = query
        self.kwargs = kwargs
        pass

    def get_last_query(self):
        return self.query

    def get_last_kwargs(self):
        return self.kwargs


def test_receive_event_new_email():
    path_config = str(pathlib.Path(__file__).parent.resolve()) + '/test-config.yaml'
    path_mail = str(pathlib.Path(__file__).parent.resolve()) + '/data/80252.emlx'

    # load config
    config = YamlConfig(path_config)
    config.load()

    service = DatabaseService(config)
    service.driver = MockDriver()

    email = EmlxMail.from_file(path_mail)
    event = EmbeddingNewMailEvent(email)
    service.handle_new_mail(event)

    assert service.driver.driver_session.get_last_query() == "MERGE (m:Mail {message_id: $message_id, file_name: $file_name, subject: $subject, date: $date, attachments: $attachments, replying_to_message_id: $replying_to_message_id})"
    assert service.driver.driver_session.get_last_kwargs() == {
        'message_id': '30773215.20230925102749.651160a5beeb48.30738213@mail136-2.atl41.mandrillapp.com',
        'file_name': path_mail, 'subject': 'Only 3 days left on your free trial of Loom Business',
        'date': datetime.datetime(2023, 9, 25, 10, 27, 49, tzinfo=tzutc()), 'attachments': 0,
        'replying_to_message_id': 'NONE'}


def test_receive_event_new_user():
    path_config = str(pathlib.Path(__file__).parent.resolve()) + '/test-config.yaml'

    # load config
    config = YamlConfig(path_config)
    config.load()

    service = DatabaseService(config)
    service.driver = MockDriver()

    user = Participant("John Doe", "john.doe@example.com")
    event = EmbeddingNewUserEvent(user)
    service.handle_new_user(event)

    assert service.driver.driver_session.get_last_query() == "MERGE (u:User {name: $name, email: $email})"
    assert service.driver.driver_session.get_last_kwargs() == {"name": "John Doe", "email": "john.doe@example.com"}

def test_receive_event_new_domain():
    path_config = str(pathlib.Path(__file__).parent.resolve()) + '/test-config.yaml'

    # load config
    config = YamlConfig(path_config)
    config.load()

    service = DatabaseService(config)
    service.driver = MockDriver()

    domain = EmbeddingNewDomainEvent("domain.dd")
    service.handle_new_domain(domain)

    assert service.driver.driver_session.get_last_query() == "MERGE (d:Domain {name: $name})"
    assert service.driver.driver_session.get_last_kwargs() == {"name": "domain.dd"}
