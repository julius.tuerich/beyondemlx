import re

from beyondemlx.anonymizer.steps.Step import Step
from beyondemlx.emlx.EmlxMail import EmlxMail


class CopyAndAnonymizeCC(Step):

    def execute(self, mail: EmlxMail, anonymized_mail: EmlxMail) -> EmlxMail:
        """
        Replace the cc header of the mail with a new cc header.
        """
        receivers = mail.get_cc()

        anonymized_header = ''

        for receiver in receivers:
            name = self.faker.name(receiver.get_name())
            email = self.faker.email(receiver.get_email())
            anonymized_header += name + ' <' + email + '>, '

        if anonymized_header.endswith(', '):
            anonymized_header = anonymized_header[:-2]

        anonymized_mail.add_header('Cc', anonymized_header)

        return mail
