import re

from beyondemlx.anonymizer.steps.Step import Step
from beyondemlx.emlx.EmlxMail import EmlxMail


class CopyAndAnonymizeBCC(Step):

    def execute(self, mail: EmlxMail, anonymized_mail: EmlxMail) -> EmlxMail:
        """
        Replace the bcc header of the mail with a new bcc header.
        """
        receivers = mail.get_bcc()

        anonymized_header = ''

        for receiver in receivers:
            name = self.faker.name(receiver.get_name())
            email = self.faker.email(receiver.get_email())
            anonymized_header += name + ' <' + email + '>, '

        if anonymized_header.endswith(', '):
            anonymized_header = anonymized_header[:-2]

        anonymized_mail.add_header('Bcc', anonymized_header)

        return mail
