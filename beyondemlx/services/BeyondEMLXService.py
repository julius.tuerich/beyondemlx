import logging
import time

from pydispatch import dispatcher

from beyondemlx.config.YamlRunConfig import YamlRunConfig
from beyondemlx.events import Events
from beyondemlx.events.File.FileNewMailboxEvent import FileNewMailboxEvent
from beyondemlx.services.ApiService import ApiService
from beyondemlx.services.DatabaseService import DatabaseService
from beyondemlx.services.EmbeddingService import EmbeddingService
from beyondemlx.services.FileWatchService import FileWatchService
from beyondemlx.services.MailClientService import MailClientService
from beyondemlx.services.Service import Service


class BeyondEMLXService(Service):
    def __init__(self, config: YamlRunConfig):
        self.services = {}

        # set config
        self.config = config

    def start(self, command: str = None):
        """
        Starts the service.
        """
        # set up logging
        self.set_up_logging()

        # create services
        self.create_services()

        # set up event bus
        self.set_up_event_bus()

        # send start event
        dispatcher.send(Events.EVENT_SYSTEM_START, sender=self)

        # start services
        self.start_services()

        # register input loop
        if command is None:
            self.input_loop()
        else:
            self._handle_command(command)

        # send kill event
        dispatcher.send(Events.EVENT_SYSTEM_KILL, sender=self)

    def set_up_logging(self):
        """
        Sets up the logging.
        """
        level = logging.WARNING
        config_level = self.config.get("LOG_LEVEL")

        if config_level == "DEBUG":
            level = logging.DEBUG
        elif config_level == "INFO":
            level = logging.INFO
        elif config_level == "WARNING":
            level = logging.WARNING
        elif config_level == "ERROR":
            level = logging.ERROR
        elif config_level == "CRITICAL":
            level = logging.CRITICAL
        else:
            logging.warning("Unknown log level: " + config_level)

        logging.getLogger().setLevel(level)

    def create_services(self):
        """
        Creates the sub services.
        """
        self.services["file_watch_service"] = FileWatchService(self.config)
        self.services["embedding_service"] = EmbeddingService(self.config)
        self.services["database_service"] = DatabaseService(self.config)
        self.services["api_service"] = ApiService(self.config, self.services["database_service"])
        self.services["mail_client_service"] = MailClientService(self.config)

    def set_up_event_bus(self):
        """
        Lets the services register their events.
        """
        for _, service in self.services.items():
            service.register_events()

    def start_services(self):
        """
        Starts the services.
        """
        for _, service in self.services.items():
            service.start()

    def input_loop(self):
        """
        Starts the input loop.
        """
        while True:
            time.sleep(.25)
            command = input("> ")
            result = self._handle_command(command)
            if result == 1:
                break
            elif result is not None:
                print(result)

    def _handle_command(self, command: str) -> int|str|None:
        commands = ["help", "exit", "server:start", "db:wipe", "mailbox:init"]

        if command == "server:start":
            self._system_start_server()
        elif command == "exit":
            return 1
        elif command == "db:wipe":
            self._system_wipe_db()
        elif command == "mailbox:init":
            self._system_init_mailbox()
        elif command == "help":
            response = "Available commands:"
            for command in commands:
                response += "\n" + (" - " + command)
            return response
        else:
            return "Unknown command. Enter 'help' to see available commands."

    def _system_start_server(self):
        """
        Starts the server.
        """
        dispatcher.send(Events.EVENT_SYSTEM_API_START, sender=self)

    def _system_wipe_db(self):
        """
        Wipes the database.
        """
        dispatcher.send(Events.EVENT_SYSTEM_DB_WIPE, sender=self)

    def _system_init_mailbox(self):
        """
        Initializes the mailbox.
        """
        dispatcher.send(Events.EVENT_SYSTEM_MAILBOX_INIT, sender=self,
                        event=FileNewMailboxEvent(self.config.get("MAIL_DIRECTORY")))