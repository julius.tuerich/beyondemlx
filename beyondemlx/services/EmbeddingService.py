import logging
import typing

from pydispatch import dispatcher
from pathlib import Path

from beyondemlx.config.YamlRunConfig import YamlRunConfig
from beyondemlx.emlx.EmlxMail import EmlxMail
from beyondemlx.events import Events
from beyondemlx.events.Embedding.EmbeddingNewDomainEvent import EmbeddingNewDomainEvent
from beyondemlx.events.Embedding.EmbeddingNewMailEvent import EmbeddingNewMailEvent
from beyondemlx.events.Embedding.EmbeddingNewUserDomainRelationEvent import EmbeddingNewUserDomainRelationEvent
from beyondemlx.events.Embedding.EmbeddingNewUserEmailRelationEvent import EmbeddingNewUserEmailRelationEvent
from beyondemlx.events.Embedding.EmbeddingNewUserEvent import EmbeddingNewUserEvent
from beyondemlx.events.File.FileCeatedEvent import FileCreatedEvent
from beyondemlx.events.File.FileMovedEvent import FileMovedEvent
from beyondemlx.events.File.FileDeletedEvent import FileDeletedEvent
from beyondemlx.events.File.FileNewMailboxEvent import FileNewMailboxEvent
from beyondemlx.services.Service import Service


class EmbeddingService(Service):

    def __init__(self, config: YamlRunConfig):
        self.config = config

    def register_events(self):
        """
        Registers the event to listen to.
        """
        dispatcher.connect(self.handle_new_mailbox, signal=Events.EVENT_SYSTEM_MAILBOX_INIT, sender=dispatcher.Any)
        dispatcher.connect(self.handle_new_emlx_mail, signal=Events.EVENT_FILE_NEW_EMLX_MAIL, sender=dispatcher.Any)
        dispatcher.connect(self.handle_moved_emlx_mail, signal=Events.EVENT_FILE_MOVED_EMLX_MAIL, sender=dispatcher.Any)

    def handle_new_mailbox(self, event: FileNewMailboxEvent):
        """
        Handles a new mailbox.
        """
        logging.info("EmbeddingService.handle_new_mailbox")

        # get mailbox path
        mailbox_path = event.src_path

        # get all emlx files from mailbox recursively
        paths = list(Path(mailbox_path).rglob("*.[eE][mM][lL][xX]"))
        emlx_mails = [EmlxMail.from_file(str(path)) for path in paths]

        # embed mails
        self.embed_mails(emlx_mails)

    def handle_new_emlx_mail(self, event: FileCreatedEvent):
        """
        Handles a new emlx mail.
        """
        mail = EmlxMail.from_file(event.get_src_path())
        self.embed_mails([mail])

    def handle_moved_emlx_mail(self, event: FileMovedEvent):
        """
        Handles a moved emlx mail.
        """
        # just delete the old mail
        dispatcher.send(Events.EVENT_FILE_DELETED_EMLX_MAIL, sender=self, event=FileDeletedEvent(event.dest_path))

    def embed_mails(self, emlx_mails: typing.List[EmlxMail]):
        """
        Embeds the mails and sends the corresponding events.
        """

        domains = set()
        users = {}
        mails = set()
        for mail in emlx_mails:
            # skip mails without necessary headers (i.e. calendar files)
            if not mail.has_necessary_headers():
                continue

            # add mail
            mails.add(mail)

            # add participants
            participants = mail.get_from() + mail.get_to() + mail.get_cc() + mail.get_bcc()
            for participant in participants:
                # add domain
                domains.add(participant.get_domain())

                # add user
                if participant.get_email() not in users:
                    users[participant.get_email()] = participant

        # dispatch domain events
        for domain in domains:
            dispatcher.send(Events.EVENT_EMBEDDING_NEW_DOMAIN, sender=self, event=EmbeddingNewDomainEvent(domain))

        # dispatch user events
        for user in users.values():
            dispatcher.send(Events.EVENT_EMBEDDING_NEW_USER, sender=self, event=EmbeddingNewUserEvent(user))
            dispatcher.send(Events.EVENT_EMBEDDING_NEW_USER_DOMAIN_RELATION, sender=self,
                            event=EmbeddingNewUserDomainRelationEvent(user, user.get_domain(), "BELONGS_TO"))

        # dispatch mail events
        for mail in mails:
            dispatcher.send(Events.EVENT_EMBEDDING_NEW_MAIL, sender=self, event=EmbeddingNewMailEvent(mail))

            for participant in mail.get_from():
                dispatcher.send(Events.EVENT_EMBEDDING_NEW_USER_EMAIL_RELATION, sender=self,
                                event=EmbeddingNewUserEmailRelationEvent(participant, mail, "IS_FROM"))

            for participant in mail.get_to():
                dispatcher.send(Events.EVENT_EMBEDDING_NEW_USER_EMAIL_RELATION, sender=self,
                                event=EmbeddingNewUserEmailRelationEvent(participant, mail, "IS_TO"))

            for participant in mail.get_cc():
                dispatcher.send(Events.EVENT_EMBEDDING_NEW_USER_EMAIL_RELATION, sender=self,
                                event=EmbeddingNewUserEmailRelationEvent(participant, mail, "IS_CC"))

            for participant in mail.get_bcc():
                dispatcher.send(Events.EVENT_EMBEDDING_NEW_USER_EMAIL_RELATION, sender=self,
                                event=EmbeddingNewUserEmailRelationEvent(participant, mail, "IS_BCC"))