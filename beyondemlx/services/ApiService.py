import logging
import subprocess

import flask
from pydispatch import dispatcher
from flask import Flask, request
from flask_cors import cross_origin

from beyondemlx.config.YamlRunConfig import YamlRunConfig
from beyondemlx.events import Events
from beyondemlx.events.Database.DatabaseDeletedRecordEvent import DatabaseDeletedRecordEvent
from beyondemlx.events.Database.DatabaseNewRecordEvent import DatabaseNewRecordEvent
from beyondemlx.events.Mail.MailOpenedEvent import MailOpenedEvent
from beyondemlx.services.DatabaseService import DatabaseService
from beyondemlx.services.Service import Service


class ApiService(Service):

    def __init__(self, config: YamlRunConfig, database_service: DatabaseService):
        self.config = config
        self.database_service = database_service
        self.flask_app = Flask(__name__)

    def register_events(self):
        """
        Registers the event to listen to.
        """
        dispatcher.connect(self.handle_new_database_record, signal=Events.EVENT_DATABASE_NEW_RECORD)
        dispatcher.connect(self.handle_deleted_database_record, signal=Events.EVENT_DATABASE_DELETED_RECORD)
        dispatcher.connect(self.start_flask_server, signal=Events.EVENT_SYSTEM_API_START)

    def start(self):
        """
        Starts the service.
        """
        logging.info("APIService.start")

        self.flask_app.add_url_rule('/api/open-mail', 'open_mail', self.open_mail, methods=['POST', 'OPTIONS'])
        self.flask_app.add_url_rule('/api/mail-directory', 'mail_directory', self.mail_directory, methods=['POST', 'OPTIONS'])
        self.flask_app.add_url_rule('/api/query', 'query', self.execute_query, methods=['POST', 'OPTIONS'])

    def start_flask_server(self):
        """
        Kills the flask thread.
        """
        self.flask_app.run(host=self.config.get('API_HOST'), port=self.config.get('API_PORT'))

    def handle_new_database_record(self, event: DatabaseNewRecordEvent):
        """
        Handles a new database record.
        """
        logging.info("APIService.handle_new_database_record")

    def handle_deleted_database_record(self, event: DatabaseDeletedRecordEvent):
        """
        Handles a deleted database record.
        """
        logging.info("APIService.handle_deleted_database_record")

    def _send_response(self, data):
        """
        Sends a response.
        """
        response = flask.jsonify(data)
        return response

    @cross_origin()
    def mail_directory(self):
        """
        Returns the mailbox path.
        """
        return self._send_response({
            'meta': {
                'code': 200,
                'status': 'success',
                'message': 'Mailbox path retrieved successfully.'
            },
            'data': {
                'path': self.config.get('MAIL_DIRECTORY')
            }
        })

    @cross_origin()
    def open_mail(self):
        """
        Test endpoint.
        """
        path = request.json.get('path')

        if path is None:
            return self._send_response({
                'meta': {
                    'code': 400,
                    'status': 'error',
                    'message': 'No path provided.'
                }
            })

        # check if file exists with standard library
        try:
            with open(path) as f:
                pass
        except IOError:
            return self._send_response({
                'meta': {
                    'code': 400,
                    'status': 'error',
                    'message': 'File does not exist.'
                }
            })

        # open mail
        dispatcher.send(Events.EVENT_MAIL_OPENED, sender=self, event=MailOpenedEvent(path))

        return self._send_response({
            'meta': {
                'code': 200,
                'status': 'success',
                'message': 'Mail opened successfully.'
            }
        })

    @cross_origin()
    def execute_query(self):
        """
        Query endpoint.
        """
        query = request.json.get('query')

        if query is None:
            return {
                'meta': {
                    'code': 400,
                    'status': 'error',
                    'message': 'No query provided.'
                }
            }

        # execute query
        result = self.database_service.execute_query(query)

        nodes = []
        relationships = []

        for node in result.nodes:
            new_node = {
                'element_id': node.element_id,
                'labels': list(node.labels),
                'properties': dict(node)
            }
            # convert any date objects to string
            for key, value in new_node['properties'].items():
                if hasattr(value, 'isoformat'):
                    new_node['properties'][key] = value.isoformat()
            nodes.append(new_node)

        for relationship in result.relationships:
            relationships.append({
                'element_id': relationship.element_id,
                'type': relationship.type,
                'properties': dict(relationship),
                'start_node': relationship.start_node.element_id,
                'end_node': relationship.end_node.element_id
            })

        return self._send_response({
            'meta': {
                'code': 200,
                'status': 'success',
                'message': 'Query executed successfully.'
            },
            'data': {
                'nodes': nodes,
                'relationships': relationships
            }
        })
