import setuptools

with open('README.md', 'r', encoding='utf-8') as fh:
    long_description = fh.read()

setuptools.setup(
    name='beyondemlx',
    author='Julius Türich',
    author_email='julius.tuerich@uni-bielefeld.de',
    description='Beyond MBOX Package',
    keywords='mbox, emlx, email, embedding, clustering, beyondemlx',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='',
    project_urls={
        'Documentation': '',
        'Bug Reports': '',
        'Source Code': '',
    },
    package_dir={"beyondemlx": "beyondemlx"},
    packages=setuptools.find_packages(where='src'),
    classifiers=[
        # see https://pypi.org/classifiers/
        'Development Status :: 5 - Production/Stable',

        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',

        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3 :: Only',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    python_requires='>=3.9',
    install_requires=['faker', 'pytest', 'PyDispatcher', 'pyyaml', 'watchdog', 'neo4j', 'Flask', 'flask-cors', 'python-dateutil', 'jupyter', 'matplotlib', 'seaborn'],
    # entry_points={
    #     'console_scripts': [  # This can provide executable scripts
    #         'run=examplepy:main',
    # You can execute `run` in bash to run `main()` in src/examplepy/__init__.py
    #     ],
    # },
)
