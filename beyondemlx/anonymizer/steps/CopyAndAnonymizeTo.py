import re

from beyondemlx.anonymizer.steps.Step import Step
from beyondemlx.emlx.EmlxMail import EmlxMail


class CopyAndAnonymizeTo(Step):

    def execute(self, mail: EmlxMail, anonymized_mail: EmlxMail) -> EmlxMail:
        """
        Replace the to header of the mail with a new to header.
        """
        receivers = mail.get_to()

        anonymized_header = ''

        for receiver in receivers:
            name = self.faker.name(receiver.get_name())
            email = self.faker.email(receiver.get_email())
            anonymized_header += name + ' <' + email + '>, '

        if anonymized_header.endswith(', '):
            anonymized_header = anonymized_header[:-2]

        anonymized_mail.add_header('To', anonymized_header)

        return mail
