
class Transformer {
    api2graph(data: any, originD: any = null){
        return {
            nodes: data.nodes.map((node: any) => {
                return {
                    ...node,
                    id: node.element_id,
                    r: originD?.r ?? 0,
                    x: originD?.x ?? 0,
                    y: originD?.y ?? 0,
                    fx: originD?.fx ?? null,
                    fy: originD?.fy ?? null,
                    vx: originD?.vx ?? 0,
                    vy: originD?.vy ?? 0,
                    color: "blue",
                }
            }),
            relationships: data.relationships.map((relationship: any) => {
                return {
                    ...relationship,
                    id: relationship.element_id,
                    source: relationship.start_node,
                    target: relationship.end_node,
                };
            })
        }
    }
}

export const transformer = new Transformer();