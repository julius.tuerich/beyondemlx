import sys

# add parent directory to path
sys.path.append('../beyondemlx')

from beyondemlx.config.YamlRunConfig import YamlRunConfig
from beyondemlx.services.BeyondEMLXService import BeyondEMLXService



if __name__ == "__main__":
    # check if yaml file is provided
    if len(sys.argv) < 2:
        print("Use: python run.py <config.yaml> [command]")
        exit(1)

    # get config from yaml file
    config = YamlRunConfig(sys.argv[1])
    config.load()

    # get command if provided
    command = None
    if len(sys.argv) > 2:
        command = sys.argv[2]

    # build and start service
    service = BeyondEMLXService(config)
    service.start(command)