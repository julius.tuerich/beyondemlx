from abc import ABC

from beyondemlx.events.File.FileEvent import FileEvent


class FileDeletedEvent(FileEvent):
    pass
