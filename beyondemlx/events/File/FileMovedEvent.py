from abc import ABC

from beyondemlx.events.File.FileEvent import FileEvent


class FileMovedEvent(FileEvent):

    def __init__(self, src_path: str, dest_path: str):
        super().__init__(src_path)
        self.dest_path = dest_path

    def get_dest_path(self) -> str:
        """
        Returns the destination path.
        """
        return self.dest_path
