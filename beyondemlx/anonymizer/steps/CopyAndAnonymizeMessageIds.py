import re

from beyondemlx.anonymizer.steps.Step import Step
from beyondemlx.emlx.EmlxMail import EmlxMail


class CopyAndAnonymizeMessageIds(Step):

    def execute(self, mail: EmlxMail, anonymized_mail: EmlxMail) -> EmlxMail:
        """
        Replace the message id of the mail with a new one.
        """
        message_id = mail.get_message_id()
        replying_message_id = mail.get_replying_to_message_id()

        anonymized_message_id = self.faker.message_id(message_id)
        anonymized_mail.add_header('Message-Id', anonymized_message_id)

        if replying_message_id is not None:
            anonymized_replying_message_id = self.faker.message_id(replying_message_id)
            anonymized_mail.add_header('In-Reply-To', anonymized_replying_message_id)

        return mail
