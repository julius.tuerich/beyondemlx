def get_address_from_header(header) -> str:
    """
    Get the address from a header.
    """
    if '<' in header and '>' in header:
        return header.split('<')[1].split('>')[0]

    words = header.split(' ')

    if len(words) == 1:
        return words[0]

    return words[-1]