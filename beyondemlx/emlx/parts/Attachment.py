import os


class Attachment(object):
    def __init__(self, path):
        """
        Create an attachment object.
        """
        self.path = path

    def get_path(self) -> str:
        """
        Get the path of the attachment.
        """
        return self.path

    def get_name(self) -> str:
        """
        Get the name of the attachment.
        """
        return os.path.basename(self.path)

    def get_extension(self) -> str:
        """
        Get the extension of the attachment.
        """
        return os.path.splitext(self.path)[1]

    def get_size(self) -> int:
        """
         Get the size of the attachment.
        """
        return os.path.getsize(self.path)

    def get_content(self) -> bytes:
        """
        Get the content of the attachment.
        """
        with open(self.path, 'rb') as f:
            return f.read()

    def set_content(self, content):
        """
        Set the content of the attachment.
        """
        with open(self.path, 'wb') as f:
            f.write(content)