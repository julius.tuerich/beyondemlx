import re
import os
import typing
import glob
from datetime import datetime

from beyondemlx.emlx.header_utils import get_address_from_header
from beyondemlx.emlx.parts.Attachment import Attachment
from beyondemlx.emlx.parts.Participant import Participant
from beyondemlx.emlx.path_utils import get_files_recursively
from beyondemlx.emlx.unicode_utils import decode_rfc_1342


class EmlxMail(object):

    def __init__(self):
        """
        Create a new emlx mail from a file.
        """
        self.path = None
        self.content = ""
        self.original_content = ""

    @staticmethod
    def from_file(path) -> 'EmlxMail':
        """
        Create a new emlx mail from a file.
        """
        with open(path, 'r', encoding='latin-1') as f:
            content = f.read()

        mail = EmlxMail()
        mail.path = path
        mail.content = content
        mail.original_content = content

        return mail

    def get_path(self) -> str:
        """
        Get the path of the emlx file.
        """
        return self.path

    def write_to_disk(self, path):
        """
        Write the content of the emlx file to the disk. If no path is given, the original path is used.
        """
        with open(path, 'w', encoding='latin-1') as f:
            f.write(self.content)

    def get_content(self):
        """
        Get the content of the mail. May be modified.
        """
        return self.content

    def set_content(self, content):
        """
        Set the content of the mail.
        """
        self.content = content

    def is_modified(self) -> bool:
        """
        Check if the content of the mail has been modified.
        """
        return self.content != self.original_content

    def get_body_length(self) -> int:
        """
        Get the length of the body of the mail.
        """
        content = self.get_content()
        length = int(content.split('\n')[0])
        return length

    def get_body(self) -> str:
        """
        Get the body of the mail.
        """
        content = self.get_content()
        start = content.find('\n\n')
        body = content[start + 2:]
        return body

    def set_body(self, body):
        """
        Replace the body of the mail.
        """
        content = self.get_content()
        new_content = content.split('\n\n')[0] + '\n\n' + body
        self.set_content(new_content)

    def get_header(self, header) -> str | None:
        """
        Get a header of the email.
        """
        content = self.get_content()
        header_content = None

        # remove body from content
        content = content.split('\n\n')[0]

        added_to_header_content = False

        for line in content.split('\n'):
            # check if header content is also next line
            if added_to_header_content and (line.startswith('	') or line.startswith(' ')):
                header_content += " " + decode_rfc_1342(line[1:])
            else:
                added_to_header_content = False
                # if header content is fully read return it
                if header_content is not None:
                    return header_content

            # check if start of header is found
            if line.lower().startswith(header.lower() + ':'):
                line = decode_rfc_1342(line)
                header_content = line.replace(header + ':', '', 1).lstrip()
                added_to_header_content = True

        return header_content

    def replace_header(self, header, value):
        """
        Replaces a header of the email.
        """
        if self.get_header(header) is None:
            raise ValueError('Header ' + header + ' not found.')

        content = self.get_content()
        new_content = ''

        for line in content.split('\n'):
            if line.startswith(header + ': '):
                line = header + ': ' + value
                line_found = True
            new_content += line + '\n'

        self.set_content(new_content)

    def add_header(self, header, value):
        """
        Adds a header before the body.
        """
        if value is None or value == '':
            return

        content = self.get_content()

        body_start = content.find('\n\n')

        if body_start == -1:
            body_start = len(content)

        new_content = content[:body_start] + '\n' + header + ': ' + value + '\n\n' + content[body_start + 2:]
        self.set_content(new_content)

    def set_header(self, header: str, value: str):
        """
        Adds or replaces a header
        """
        if self.get_header(header) is None:
            self.add_header(header, value)
        else:
            self.replace_header(header, value)

    def get_from(self) -> typing.List[Participant]:
        """
        Get the senders of the mail.
        """
        return self.get_participants_from_header('From')

    def get_to(self) -> typing.List[Participant]:
        """
        Get the recipients of the mail.
        """
        return self.get_participants_from_header('To')

    def get_cc(self) -> typing.List[Participant]:
        """
        Get the recipients of the mail.
        """
        return self.get_participants_from_header('Cc')

    def get_bcc(self) -> typing.List[Participant]:
        """
        Get the recipients of the mail.
        """
        return self.get_participants_from_header('Bcc')

    def get_participants_from_header(self, header: str) -> typing.List[Participant]:
        """
        Get the participants from a header.
        """
        header = self.get_header(header)

        if header is None:
            return []

        if not "@" in header:
            return []

        # split at commas which are not inside of "
        parts = re.split(r',(?=(?:[^"]*"[^"]*")*[^"]*$)', header)
        participants = []

        for part in parts:
            if not '@' in part:
                continue
            participants.append(Participant.from_string(part))

        return participants

    def get_subject(self) -> str | None:
        """
        Get the subject of the mail.
        """
        header = self.get_header('Subject')
        return decode_rfc_1342(header)

    def get_date(self) -> datetime | None:
        """
        Get the date of the mail.
        """
        header = self.get_header('Date')
        if header is None:
            return None

        try:
            return datetime.strptime(header, '%a, %d %b %Y %H:%M:%S %z')
        except ValueError:
            return None

    def get_message_id(self) -> str | None:
        """
        Get the message id of the mail.
        """
        header = self.get_header('Message-Id')
        return self._format_message_id(header) if header is not None else None

    def get_replying_to_message_id(self) -> str | None:
        """
        Get the message id of the mail.
        """
        header = self.get_header('In-Reply-To')
        if header is None:
            header = self.get_header('References')
        if header is None:
            header = self.get_header('Original-Message-ID')

        return self._format_message_id(header) if header is not None else None

    def _format_message_id(self, message_id: str) -> str:
        """
        Format a message id.
        """
        # get the message id between < and >
        if '<' in message_id:
            message_id = message_id.split('<')[1]
        if '>' in message_id:
            message_id = message_id.split('>')[0]
        return message_id

    def has_necessary_headers(self) -> bool:
        """
        Check if the mail has the necessary headers.
        """
        return self.get_message_id() is not None and self.get_subject() is not None and len(self.get_from()) > 0 and len(self.get_to()) > 0

    def get_attachment_base_folder(self) -> str:
        """
        Get the folder where the attachments of all mails in this directory are stored.
        """
        message_folder = os.path.dirname(self.path)

        # remove last folder from message_folder
        message_folder = '/'.join(message_folder.split("/")[:-1])

        # build attachment folder
        return os.path.join(message_folder, 'Attachments')

    def get_attachment_folder(self) -> str:
        """
        Get the folder where the attachments of the mail are stored.
        """
        # get the id of the mail
        emlx_id = os.path.basename(self.path).split('.')[0]

        # get the message folder

        attachment_folder = os.path.join(self.get_attachment_base_folder(), emlx_id)
        return attachment_folder

    def has_attachments(self) -> bool:
        """
        Check if the mail has attachments.
        """
        return len(self.get_attachments()) > 0

    def get_attachments(self) -> typing.List[Attachment]:
        """
        Get the attachments of the mail.
        """
        attachment_folder = self.get_attachment_folder()

        if not os.path.exists(attachment_folder):
            return []

        files = get_files_recursively(attachment_folder)
        return [Attachment(f) for f in files]

    def to_dict(self):
        """
        Convert the mail to a dictionary.
        """
        return {
            'date': self.get_header('Date'),
            'from': [p.to_dict() for p in self.get_from()],
            'to': [p.to_dict() for p in self.get_to()],
            'cc': [p.to_dict() for p in self.get_cc()],
            'bcc': [p.to_dict() for p in self.get_bcc()],
            'subject': self.get_subject(),
            'message_id': self.get_message_id(),
            'replying_to_message_id': self.get_replying_to_message_id(),
            #   'body': self.get_body(),
            #   'attachments': [a.to_dict() for a in self.get_attachments()]
        }
