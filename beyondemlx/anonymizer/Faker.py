import faker
import hashlib

class Faker(object):

    def __init__(self):
        self.faker = faker.Faker()

    def seed_deterministic(self, string: str):
        """
        Seed the faker with a deterministic value.
        """
        hash_number = int(hashlib.sha512(string.encode('utf-8')).hexdigest(), 16)
        self.faker.seed_instance(seed=hash_number)

    def first_name(self, name: str) -> str:
        """
        Generates a first name.
        """
        self.seed_deterministic(name)
        return self.faker.first_name()

    def last_name(self, name: str) -> str:
        """
        Generates a last name.
        """
        self.seed_deterministic(name)
        return self.faker.last_name()

    def name(self, name: str) -> str:
        """
        Generates a full name.
        """
        self.seed_deterministic(name)
        return self.faker.name()

    def email(self, email: str) -> str:
        """
        Generates an email address. Same email domains will also be mapped to the same fake domain.
        """

        parts = email.split('@')

        username = parts[0]
        domain = parts[1]

        self.seed_deterministic(username)
        fake_username = self.faker.email().split('@')[0]
        self.seed_deterministic(domain)
        fake_domain = self.faker.domain_name()

        return fake_username + '@' + fake_domain


    def message_id(self, email: str) -> str:
        """
        Generates a message id. Same email domains will also be mapped to the same fake domain.
        """

        parts = email.split('@')

        message_id = parts[0]
        domain = parts[1]

        self.seed_deterministic(message_id)
        fake_message_id = self.faker.uuid4()
        self.seed_deterministic(domain)
        fake_domain = self.faker.domain_name()

        return fake_message_id + '@' + fake_domain

    def ipv4(self, ipv4: str) -> str:
        """
        Generates an ipv4 address.
        """
        self.seed_deterministic(ipv4)
        return self.faker.ipv4()

    def sentence(self, sentence: str) -> str:
        """
        Generates a sentence.
        """
        self.seed_deterministic(sentence)
        return self.faker.sentence()

    def subject(self, subject: str) -> str:
        """
        Generates a subject and keeps the prefixes.
        """
        allowed_prefixes = ['Re', 'RE', 'Fwd', 'FWD', 'Fw', 'FW']
        for i in range(0, len(allowed_prefixes)):
            prefix = allowed_prefixes[i]
            allowed_prefixes.append(prefix + ":")

        words = subject.split()
        used_prefixes = []
        remaining_subject = ''

        for word in words:
            if word in allowed_prefixes:
                used_prefixes.append(word)
            else:
                remaining_subject += word + ' '

        hashed_remaining_subject = self.sentence(remaining_subject)
        hashed_subject = ''
        if len(used_prefixes) > 0:
            hashed_subject = ' '.join(used_prefixes) + ' '
        hashed_subject += hashed_remaining_subject
        return hashed_subject

    def body(self, body: str) -> str:
        """
        Generates a body and keeps the length of the original body.
        """
        length = len(body)

        if length == 0:
            return ''

        self.seed_deterministic(body)

        new_body = ''

        while len(new_body) < length:
            new_body += self.faker.sentence() + ' '

        return new_body[:length-1] + '.'

