import datetime

from dateutil.tz import tzutc
from pydispatch import dispatcher

from beyondemlx.emlx.EmlxMail import EmlxMail
from beyondemlx.emlx.parts.Participant import Participant
from beyondemlx.events import Events
from beyondemlx.events.Embedding.EmbeddingNewDomainEvent import EmbeddingNewDomainEvent
from beyondemlx.events.Embedding.EmbeddingNewMailEvent import EmbeddingNewMailEvent
from beyondemlx.events.Embedding.EmbeddingNewUserEvent import EmbeddingNewUserEvent
from beyondemlx.events.File.FileNewMailboxEvent import FileNewMailboxEvent
from beyondemlx.services.DatabaseService import DatabaseService
from beyondemlx.config.YamlConfig import YamlConfig
import pathlib

from beyondemlx.services.EmbeddingService import EmbeddingService

def assert_event_new_domain(**data):
    assert data['signal'] == Events.EVENT_EMBEDDING_NEW_DOMAIN
    assert data['event'].domain in ['loom.com', 'uni-bielefeld.de']

def assert_event_new_user(**data):
    assert data['signal'] == Events.EVENT_EMBEDDING_NEW_USER
    assert data['event'].user.email in ['no-reply@loom.com', 'office@uni-bielefeld.de']

def assert_event_new_mail(**data):
    assert data['signal'] == Events.EVENT_EMBEDDING_NEW_MAIL
    assert data['event'].mail.get_subject() == 'Only 3 days left on your free trial of Loom Business'

def test_emit_new_mailbox_domain():
    path_config = str(pathlib.Path(__file__).parent.resolve()) + '/test-config.yaml'
    path_mail = str(pathlib.Path(__file__).parent.resolve()) + '/data'

    # load config
    config = YamlConfig(path_config)
    config.load()

    service = EmbeddingService(config)

    # connect events
    dispatcher.connect(assert_event_new_domain, signal=Events.EVENT_EMBEDDING_NEW_DOMAIN)

    event = FileNewMailboxEvent(path_mail)
    service.handle_new_mailbox(event)

def test_emit_new_mailbox_user():
    path_config = str(pathlib.Path(__file__).parent.resolve()) + '/test-config.yaml'
    path_mail = str(pathlib.Path(__file__).parent.resolve()) + '/data'

    # load config
    config = YamlConfig(path_config)
    config.load()

    service = EmbeddingService(config)

    # connect events
    dispatcher.connect(assert_event_new_user, signal=Events.EVENT_EMBEDDING_NEW_USER)

    event = FileNewMailboxEvent(path_mail)
    service.handle_new_mailbox(event)

def test_emit_new_mailbox_user():
    path_config = str(pathlib.Path(__file__).parent.resolve()) + '/test-config.yaml'
    path_mail = str(pathlib.Path(__file__).parent.resolve()) + '/data'

    # load config
    config = YamlConfig(path_config)
    config.load()

    service = EmbeddingService(config)

    # connect events
    dispatcher.connect(assert_event_new_mail, signal=Events.EVENT_EMBEDDING_NEW_MAIL)

    event = FileNewMailboxEvent(path_mail)
    service.handle_new_mailbox(event)