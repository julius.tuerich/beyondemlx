from abc import ABC

from beyondemlx.events.File.FileEvent import FileEvent


class FileCreatedEvent(FileEvent):
    pass
