from beyondemlx.config.YamlConfig import YamlConfig


class YamlRunConfig(YamlConfig):

    def is_valid(self):
        """
        Checks if the config is valid
        """
        defined_mail_directory = self.has('MAIL_DIRECTORY') and self.get('MAIL_DIRECTORY') is not None
        defined_neo4j_uri = self.has('NEO4J_URI') and self.get('NEO4J_URI') is not None
        defined_neo4j_user = self.has('NEO4J_USER') and self.get('NEO4J_USER') is not None
        defined_neo4j_password = self.has('NEO4J_PASSWORD') and self.get('NEO4J_PASSWORD') is not None
        defined_log_level = self.has('LOG_LEVEL') and self.get('LOG_LEVEL') is not None
        defined_api_host = self.has('API_HOST') and self.get('API_HOST') is not None
        defined_api_port = self.has('API_PORT') and self.get('API_PORT') is not None

        return (defined_mail_directory
                and defined_neo4j_uri
                and defined_neo4j_user
                and defined_neo4j_password
                and defined_log_level
                and defined_api_host
                and defined_api_port)
