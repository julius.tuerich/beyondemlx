import logging
import subprocess

from pydispatch import dispatcher

from beyondemlx.config.YamlRunConfig import YamlRunConfig
from beyondemlx.emlx.EmlxMail import EmlxMail
from beyondemlx.events import Events
from beyondemlx.events.Mail.MailOpenedEvent import MailOpenedEvent
from beyondemlx.services.Service import Service


class MailClientService(Service):

    def __init__(self, config: YamlRunConfig):
        self.config = config

    def register_events(self):
        """
        Registers the event to listen to.
        """
        dispatcher.connect(self.handle_mail_opened, signal=Events.EVENT_MAIL_OPENED, sender=dispatcher.Any)

    def handle_mail_opened(self, event: MailOpenedEvent):
        """
        Handles a mail opened event.
        """
        logging.info("MailClientService.handle_mail_opened")

        # open mail client
        subprocess.call(['open', event.path])
