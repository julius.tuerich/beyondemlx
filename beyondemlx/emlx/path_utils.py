import os
import typing

def get_files_recursively(folder: str) -> typing.List[str]:
    """
    Returns all files in this directory and it's subdirectory recursively.
    """
    files = []

    for entry in os.listdir(folder):
        entry = os.path.join(folder, entry)
        if os.path.isfile(entry):
            files.append(entry)
        elif os.path.isdir(entry):
            files += get_files_recursively(entry)

    return files