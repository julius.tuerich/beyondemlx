import time
import os
import watchdog.events
from pydispatch import dispatcher
from watchdog.observers import Observer

from beyondemlx.config.YamlRunConfig import YamlRunConfig
from beyondemlx.emlx.EmlxMail import EmlxMail
from beyondemlx.events import Events
from beyondemlx.events.File.FileCeatedEvent import FileCreatedEvent
from beyondemlx.events.File.FileDeletedEvent import FileDeletedEvent
from beyondemlx.events.File.FileMovedEvent import FileMovedEvent
from beyondemlx.events.File.FileNewMailboxEvent import FileNewMailboxEvent
from beyondemlx.handler.FileEventHandler import FileEventHandler
from beyondemlx.services.Service import Service


class FileWatchService(Service):

    def __init__(self, config: YamlRunConfig):
        self.config = config
        self.file_event_handler = FileEventHandler(
            self.handle_file_created,
            self.handle_file_deleted,
            self.handle_file_moved
        )
        self.observer = None

    def register_events(self):
        """
        Registers the event to listen to.
        """
        dispatcher.connect(self.unwatch_directory, signal=Events.EVENT_SYSTEM_KILL, sender=dispatcher.Any)

    def start(self):
        """
        Starts the service.
        """
        self.watch_directory()

    def watch_directory(self):
        """
        Starts watching the directory.
        """
        self.observer = Observer()
        self.observer.schedule(
            self.file_event_handler,
            self.config.get("MAIL_DIRECTORY"),
            recursive=True)
        self.observer.start()

    def unwatch_directory(self):
        """
        Stops the observer.
        """
        self.observer.stop()
        self.observer.join()

    def handle_file_created(self, event: watchdog.events.FileCreatedEvent):
        """
        Callback for when a file is created.
        """
        path = event.src_path
        if not path.endswith(".emlx"):
            return

        dispatcher.send(Events.EVENT_FILE_NEW_EMLX_MAIL, sender=self, event=FileCreatedEvent(path))

    def handle_file_deleted(self, event: watchdog.events.FileDeletedEvent):
        """
        Callback for when a file is deleted.
        """
        # check if file is an emlx file
        path = event.src_path
        if not path.endswith(".emlx"):
            return

        # check if file still exists
        file_exists = os.path.isfile(event.src_path)
        if file_exists:
            return

        dispatcher.send(Events.EVENT_FILE_DELETED_EMLX_MAIL, sender=self, event=FileDeletedEvent(path))

    def handle_file_moved(self, event: watchdog.events.FileMovedEvent):
        """
        Callback for when a file is moved.
        """
        src_path = event.src_path
        dst_path = event.dest_path
        if not src_path.endswith(".emlx") or not dst_path.endswith(".emlx"):
            return

        dispatcher.send(Events.EVENT_FILE_MOVED_EMLX_MAIL, sender=self, event=FileMovedEvent(src_path, dst_path))
