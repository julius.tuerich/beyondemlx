import sys

# add parent directory to path
sys.path.append('../beyondemlx')

from beyondemlx.config.YamlRunConfig import YamlRunConfig
from beyondemlx.services.BeyondEMLXService import BeyondEMLXService



if __name__ == "__main__":
    # check if yaml file is provided
    if len(sys.argv) < 2:
        print("Use: python integration-test.py <config.yaml>")
        exit(1)

    # get config from yaml file
    config = YamlRunConfig(sys.argv[1])
    config.load()

    # build and start service
    service = BeyondEMLXService(config)
    service.start("server:start")