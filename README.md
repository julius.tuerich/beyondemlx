# Beyond EMLX
This project was developed as part of the bachelor thesis "BeyondEMLX - An Embedding Architecture for flexible Email-Visualization and -Workflows" and accompanies the thesis. It is designed as a Python packing, following standards set by pip. The repository contains the source code for the anonymizer, the frontend and the backend, the dataset analysis notebook and the commandline scripts that help starting and testing the application.

All Python classes and methods contain PyDoc comments explaining the method in detail. Methods are type hinted to enable better understanding. This also enables better completions and suggestions from code editors and IDEs. 
The codebase follows the OOP principles, splitting the responsibilities into smaller classes representing individual components.

## Introduction
This package can either be installed with pip or cloned directly. To install the package via pip run the following command.
```bash
pip install git+https://gitlab.ub.uni-bielefeld.de/julius.tuerich/beyondemlx.git
```
If you want to clone the repository, run the `pip install .` command to install all dependencies.

To run and test the application, including frontend and backend, see section "Application".

## Structure

### /notebooks
This folder contains the notebook used for the dataset analysis. The notebook itself loads the dependencies, configuration and mails from the mailbox folder. After that four functions are used to analyze the list of emails in memory. 

The method `analyse_numbers` counts the number of unique domains, emails and users respecting their individual roles within an email.

The method `analyse_connection_numbers` plots the number of mails per number of participant respecting the participants role.

The method `analyse_shares` plots the shares of participants receiving or sending an email as pie chart.

Finally the method `analyse_heatmap` plots a heatmap between the top 10 senders and receivers visualizing the frequency of email contacts.

All results of the methods can be found in the thesis with further explanations and thoughts. 

### /beyondemlx
This folder contains the code which is used as foundation throughout the whole code base. It contains the `emlx` folder. This folder contains the code for loading and parsing emlx files, including helper functions to decode RFC encoded  headers and the classes representing the fundamental entities of an email, like participants and attachments.
The `config` folder contains the classes responsible for loading, parsing, validating and retrieving values from YAML files. YAML files are used to store the configuration of the application in a central place, the `/config` folder in the projects root directory. 
The `anonymizer` folder contains the classes responsible for replacing sensitive data from emlx files with fake data. This process is realized with an anonymization pipeline, which can also be found in this directory and consists of multiple steps, where each step is responsible for an isolated replacement of a single header. 
The `services` directory contains the main part of the backend. The backend works with independent services, each responsible for one of the following tasks. 

- Watching the file system and detecting changes in, or the creation of new emlx files.
- Loading emlx files into memory and parsing the values.
- Setup the database indices and embedding the emlx data into the database.
- Providing a REST based HTTP API the front end can interact with.
- Communication with the mail client.

The services communicate over an event bus. This event bus depends on individual events that can be emitted and contain additional data when transferred between services. These events can be found in the `events` folder. The following picture describes the architecture.

![BeyondEMLX Architecture](https://gitlab.ub.uni-bielefeld.de/julius.tuerich/beyondemlx/-/raw/master/beyondemlx-architecture.png)

Finally the `handler` folder contains the logic to handle file system changes.

### /beyondemlx-frontend
This folder is a Vue.JS project based on Vite and TypeScript. The applications components can be found in the `src/components` folder, while the logic can be found in the `src/typescript` folder. This folder contains the classes for rendering simulations, manipulating data in the rendering, the communication with the api itself and a transformer to parse the data returned by the api.

### /cli
This folder contains the command line scripts to start the application, anonymize a mailbox and a helper script for the integration tests. The `anonymize-mailbox.py` script is used to anonymize a mailbox. The `run.py` script is used to start the backend application. The `integration-test.py` script is used to run the backend for the integration tests.

To copy a mailbox and anonymize all emails in it, use the following command:
```bash
 python3 cli/anonymize-mailbox.py <input_folder> <output_folder>
```

## Application

To run the application a Mac is needed, because the application uses the emlx files produces by the Apple mail client. Also a few things need to be prepared:
- The package BeyondEMLX must be installed via pip or cloned and installed locally.
- A Neo4j database must be set up and running in the background. The simplest way to do this is to download the Neo4j Desktop application (https://neo4j.com/download/). After the applications setup, create an empty database and start the database server.
- Configure the application by creating a `run.yaml` file in the /config directory of the project root. This YAML file must contain the same variables as can be found in the example file in the folder. Enter the database connection details, credentials, api details, log level and mailbox configuration.
- Ensure that the application you use to start the backend has full access to the mailbox folder you used in the YAML config file. If you entered a path inside the `~/Library/Mail` system folder, ensure that the application you use to start the cli script (e.g. your terminal) has full disk access on your Mac. This is necessary to read files from protected mail folders and can be activated in the system settings. Alternatively copy the mailbox folder into a folder where the application has full access (e.g. your Desktop).
- Node.js and npm must be installed to run the frontend. If not installed, download and install the latest version from the official website (https://nodejs.org/en/download/).

### Start the backend

To start the backend, run the following command in the terminal. 
```bash
python3 cli/run.py config/run.yaml
```

This will start the backend and the services. The backend will connect to the database, test the connection and insert the indices. Right after that you get access to the interactive shell.
In the shell you can enter commands to interact with the backend. Type the `help` command to get a list of all available commands. You can clean the database with the `db:wipe` command and load the mailbox with the `mailbox:init` command. This gives you a fresh start and can be repeated in case of errors. Finally use the `server:start` command to start the flask API server. Leave the process active and open a new terminal window to start the frontend.

### Start the frontend
To start the frontend change the directory to the `beyondemlx-frontend` folder. 
Create a `.env` file in the root of the frontend folder. This file must contain the following line.
```bash
VITE_API_HOST="http://localhost:8080"
```
Change the value to your address of the backend, if the address the backend is served from is different.

Now run the following commands.
 
```bash
npm install
npm run dev
```

This will start the frontend. You can now open your browser and navigate to the displayed address. 
The application is now ready to use.

### Hint
This setup is a tech demonstration and not built for production use cases. From time to time (e.g. when too many requests are sent to the backend) the flask server can crash. In this case, restart the backend.

## Tests

The tests are located in the `/tests` folder. The tests are split into unit tests and integration tests. The unit tests are located in the `/tests/unit` folder and the integration tests are located in the `/tests/integration` folder. The integration tests are designed to test the whole application, including the backend and the frontend. The tests are written in Python and use the `pytest` framework. To run the tests, navigate to the project root and run the following command.

```bash
pytest
```

## Video
More information on the video can be found in the `/video/README.md` file.

## License
This project is licensed under the MIT License - see the LICENSE file for details.

## Acknowledgments
This project was developed as part of the bachelor thesis "BeyondEMLX - An Embedding Architecture for flexible Email-Visualization and -Workflows" at the Faculty of Technology at Bielefeld University. The thesis was supervised by Herr Dr. rer. nat. Thomas Hermann. The author would like to thank the supervisor for his support and guidance throughout the project. 
The author also would like to thank the open source community for providing the tools and libraries used in this project.

