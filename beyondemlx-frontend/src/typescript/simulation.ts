import * as d3 from 'd3';
import {transformer} from "@/typescript/transformer";
import {api} from "@/typescript/api";

export class Simulation {
    private svg = null
    private svgNodes: object[] = []
    private svgTexts: object[] = []
    private svgEdges: object[] = []
    private readonly width: number
    private readonly height: number
    private readonly container: HTMLElement
    private limit = 1000
    private includingReplyingMails = false
    private callbacks = {}
    private data = {
        nodes: [],
        relationships: []
    }
    private simulation: d3.Simulation<d3.SimulationNodeDatum, undefined>

    constructor(container: HTMLElement) {
        this.width = container.clientWidth
        this.height = container.clientHeight
        this.container = container

        this.simulation = d3.forceSimulation(this.data.nodes)
            .force("link", d3.forceLink(this.data.relationships).id(d => d.id))
            .force("charge", d3.forceManyBody())
            .force("x", d3.forceX())
            .force("y", d3.forceY())
            .restart()
    }

    _ticked() {
        this.svgEdges
            .attr("x1", d => d.source.x)
            .attr("y1", d => d.source.y)
            .attr("x2", d => d.target.x)
            .attr("y2", d => d.target.y)

        this.svgNodes
            .attr("cx", d => d.x)
            .attr("cy", d => d.y)

        this.svgTexts
            .attr("x", d => d.x)
            .attr("y", d => d.y)
    }

    _dragStarted(event: any, d: any) {
        if (!event.active) {
            this.simulation.alphaTarget(0.3).restart();
        }
        event.subject.fx = event.subject.x;
        event.subject.fy = event.subject.y;
    }

    _dragged(event: any, d: any) {
        event.subject.fx = event.x;
        event.subject.fy = event.y;
    }

    _dragEnded(event: any, d: any) {
        if (!event.active) {
            this.simulation.alphaTarget(0);
        }
        event.subject.fx = null;
        event.subject.fy = null;
    }

    setLimit(limit: number) {
        this.limit = limit
    }

    setIncludingReplyingMails(includingReplyingMails: boolean) {
        this.includingReplyingMails = includingReplyingMails
    }

    addData(data: {nodes: any[], relationships: any[]}): void {
        // save the data (nodes and relationships)
        data.nodes.forEach((node: any) => {
            if (this.data.nodes.some((n: any) => n.element_id == node.element_id)) {
                return
            }
            this.data.nodes.push(node)
        })
        data.relationships.forEach((relationship: any) => {
            this.data.relationships.push(relationship)
        })

        // call data added callback
        if ("data" in this.callbacks) {
            (this.callbacks["data"] as Function)(data)
        }

        // render data
        this._renderData()
    }

    clearData() {

        // clear the data
        this.data.nodes = []
        this.data.relationships = []
        this.svgTexts = []
        this.svgNodes = []
        this.svgEdges = []

        // render data
        this._renderData()
    }

    on(event: string, callback) {
        this.callbacks[event] = callback
    }

    async expandAllLeafs() {
        if (!this.svg) {
            return
        }

        const nodes = this.svg.node().querySelectorAll('circle')
        await this._simulateDoubleClickForNodes(Array.from(nodes))
    }

    async cutAllLeafs() {
        if (!this.svg) {
            return
        }

        // iterate over all relationships and find all leafs
        const leafs: {element_id: string}[] = []
        this.data.relationships.forEach((relationship: any) => {
            if (!this.data.relationships.some((r: any) => r.target == relationship.source)) {
                leafs.push(relationship.source)
            }
        })

        // remove nodes and relationships
        leafs.forEach((leaf: {element_id: string}) => {
            const index = this.data.nodes.findIndex((n: {element_id: string}) => n.element_id == leaf.element_id)
            if (index > -1) {
                this.data.nodes.splice(index, 1)
            }

            const index2 = this.data.relationships.findIndex((r: {source: {element_id: string}}) => r.source.element_id == leaf.element_id)
            if (index2 > -1) {
                this.data.relationships.splice(index2, 1)
            }
        })

        // render data
        this._renderData()
    }

    async _simulateDoubleClickForNodes(nodes: HTMLElement[]) {
        if (nodes.length === 0) {
            return
        }

        const node = nodes[0]
        const json = JSON.parse(node.getAttribute('data') as string)
        await this._handleDblClick(null, json)
        this.simulation.alphaTarget(1);
        setTimeout(() => this.simulation.alphaTarget(0), 1500)

        nodes.shift()
        await this._simulateDoubleClickForNodes(nodes)
    }

    _renderData() {
        // create new svg element
        if (this.svg === null) {
            this.svg = d3.create("svg")
                .attr("width", this.width)
                .attr("height", this.height)
                .attr("viewBox", [-this.width / 2, -this.height / 2, this.width, this.height])

            // add zoom behaviour
            const zoom = d3.zoom()
                .scaleExtent([1, 5])
                .on('zoom', (event: any) => {
                    this.svg.attr('transform', event.transform);
                });
            this.svg.call(zoom);
        } else {
            // remove all elements from the svg
            this.svg.selectAll("*").remove()
        }

        const svg = this.svg

        // get earliest and latest date
        const dates = this.data.nodes.filter((n: any) => n.labels[0] == "Mail").map((n: any) => new Date(n.properties.date))
        const dateEarliest = new Date(Math.min.apply(null, dates))
        const dateLatest = new Date(Math.max.apply(null, dates))

        // create drag behaviour
        const drag = d3.drag()
            .on("start", this._dragStarted.bind(this))
            .on("drag", this._dragged.bind(this))
            .on("end", this._dragEnded.bind(this));

        // get all old nodes and edges
        this.svgNodes = svg.selectAll("circle").data(this.data.nodes)
        this.svgTexts = svg.selectAll("text").data(this.data.nodes)
        this.svgEdges = svg.selectAll("line").data(this.data.relationships)

        // create new nodes and edges
        const enteringEdges = this.svgEdges.enter()
            .append("line")
            .lower()
            .attr("stroke", "#222")
            .attr("stroke-opacity", 0.2)
            .attr("stroke-width", 1)
            .attr("x1", d => d.source.x)
            .attr("y1", d => d.source.y)
            .attr("x2", d => d.target.x)
            .attr("y2", d => d.target.y)

        let enteringNodes = this.svgNodes.enter()
            .append('circle')
            .attr("stroke", "#000000")
            .attr("stroke-width", d => `${d.properties?.attachments > 0 ? 1 : 0}`)
            .attr("fill", d => this._getNodeColor(d, dateEarliest, dateLatest))
            .attr("class", d => `node ${d.labels.join(" ")} ${d.properties?.attachments > 0 ? 'has-attachments' : ''}`)
            .attr("r", d => 5)//make this one of three sizes
            .attr("data", d => JSON.stringify(d))
            .on('dblclick', (event: any, d: any) => this._handleDblClick(event, d))
            .on('click', (event: any, d: any) => this._handleClick(event, d))
            .call(drag)

        let enteringTexts = this.svgTexts.enter()
            .append("text")
            .attr("class", "text")
            .attr("dy", 2)
            .attr("text-anchor", "middle")
            .text(d => this._getNodeText(d))
            .on('dblclick', (event: any, d: any) => this._handleDblClick(event, d))
            .on('click', (event: any, d: any) => this._handleClick(event, d))
            .call(drag)

        // merge old and new nodes and edges
        this.svgNodes = this.svgNodes.merge(enteringNodes)
        this.svgEdges = this.svgEdges.merge(enteringEdges)
        this.svgTexts = this.svgTexts.merge(enteringTexts)

        // restart the simulation
        this.simulation = this.simulation.nodes(this.data.nodes)
            .force("link", d3.forceLink(this.data.relationships).id(d => d.id))
            .restart()

        // add tick event listener
        this.simulation
            .on("tick", this._ticked.bind(this))

        // remove all elements from the container
        while (this.container.firstChild) {
            this.container.removeChild(this.container.firstChild)
        }
        this.container.appendChild(svg.node())
    }

    _getNodeColor(d: any, dateEarliest: Date, dateLatest: Date) {
        const label = d.labels[0]

        if (label == "User") {
            return "#1f77b4"
        } else if (label == "Domain") {
            return "#ff7f0e"
        } else if (label == "Mail") {
            const startRed = 23
            const startGreen = 86
            const startBlue = 23

            const endRed = 44
            const endGreen = 160
            const endBlue = 44

            const date = new Date(d.properties.date)

            const red = Math.round(startRed + (endRed - startRed) * (date.getTime() - dateEarliest.getTime()) / (dateLatest.getTime() - dateEarliest.getTime()))
            const green = Math.round(startGreen + (endGreen - startGreen) * (date.getTime() - dateEarliest.getTime()) / (dateLatest.getTime() - dateEarliest.getTime()))
            const blue = Math.round(startBlue + (endBlue - startBlue) * (date.getTime() - dateEarliest.getTime()) / (dateLatest.getTime() - dateEarliest.getTime()))

            return `rgb(${red}, ${green}, ${blue})`
        }

        return "#000000"
    }

    _getNodeText(d: any) {
        const label = d.labels[0]

        let text = ""
        if (label == "User") {
            text = d.properties.name
        } else if (label == "Domain") {
            text = d.properties.name
        } else if (label == "Mail") {
            text = d.properties.subject
        }

        if (text.length > 20) {
            text = text.substring(0, 20) + "..."
        }

        return text
    }

    async _handleDblClick(event: Event | null, d: any) {
        if (event) {
            event.preventDefault()
            event.stopPropagation()
        }

        const id = d.element_id.split(":")[2]

        const data = transformer.api2graph(
            await api.query(`
                MATCH (n)-[r]-(m)
                WHERE ID(n)=${id}
                ${this.includingReplyingMails ? '' : 'AND ((n.replying_to_message_id IS NULL OR n.replying_to_message_id = \'NONE\') AND (m.replying_to_message_id IS NULL OR m.replying_to_message_id = \'NONE\'))'}
                RETURN r, m
                LIMIT ${this.limit}
        `), d
        )

        this.addData(data)
    }


    _handleClick(event: any, d: any) {
        if ("click" in this.callbacks) {
            (this.callbacks["click"] as Function)(event, d)
        }
    }
}