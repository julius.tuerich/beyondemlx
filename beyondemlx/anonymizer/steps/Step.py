from abc import abstractmethod

from beyondemlx.anonymizer.Faker import Faker
from beyondemlx.emlx.EmlxMail import EmlxMail


class Step(object):

    def __init__(self):
        """
        Initialize the step.
        """
        self.faker = None

    def set_faker(self, faker: Faker):
        """
        Set the faker.
        """
        self.faker = faker

    def execute(self, mail: EmlxMail, anonymized_mail: EmlxMail):
        """
        Execute the step.
        """
        raise NotImplementedError
