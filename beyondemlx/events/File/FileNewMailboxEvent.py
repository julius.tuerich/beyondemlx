from abc import ABC

from beyondemlx.events.File.FileEvent import FileEvent


class FileNewMailboxEvent(FileEvent):
    pass
