from beyondemlx.events.Database.DatabaseEvent import DatabaseEvent


class DatabaseDeletedRecordEvent(DatabaseEvent):
    pass