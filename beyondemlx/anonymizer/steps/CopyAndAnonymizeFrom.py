import re

from beyondemlx.anonymizer.steps.Step import Step
from beyondemlx.emlx.EmlxMail import EmlxMail


class CopyAndAnonymizeFrom(Step):

    def execute(self, mail: EmlxMail, anonymized_mail: EmlxMail) -> EmlxMail:
        """
        Replace the from header of the mail with a new from header.
        """
        senders = mail.get_from()

        anonymized_header = ''

        for sender in senders:
            name = self.faker.name(sender.get_name())
            email = self.faker.email(sender.get_email())
            anonymized_header += name + ' <' + email + '>, '

        if anonymized_header.endswith(', '):
            anonymized_header = anonymized_header[:-2]

        anonymized_mail.add_header('From', anonymized_header)

        return mail
