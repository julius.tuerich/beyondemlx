from beyondemlx.anonymizer.Faker import Faker
from beyondemlx.anonymizer.steps.Step import Step
from beyondemlx.emlx.EmlxMail import EmlxMail


class Pipeline(object):

    def __init__(self):
        """
        Initialize the pipeline.
        """
        self.pipeline = []
        self.faker = Faker()

    def add(self, step: Step):
        """
        Add a step to the pipeline.
        """
        step.set_faker(self.faker)
        self.pipeline.append(step)

    def execute(self, mail: EmlxMail, anonymized_mail: EmlxMail):
        """
        Execute the pipeline on a mail.
        """
        for step in self.pipeline:
            step.execute(mail, anonymized_mail)