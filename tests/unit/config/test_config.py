from beyondemlx.config.YamlConfig import YamlConfig
import pathlib

def test_faker_name():
    path = str(pathlib.Path(__file__).parent.resolve()) + '/test-config.yaml'

    # load config
    config = YamlConfig(path)
    config.load()

    assert config.get('MAIL_DIRECTORY') == '/Users/example/Library/Mail'
    assert config.get('NEO4J_URI') == 'bolt://localhost:7687'
    assert config.get('NEO4J_USER') == 'neo4j'
    assert config.get('NEO4J_PASSWORD') == 'password'
    assert config.get('LOG_LEVEL') == 'INFO'
    assert config.get('API_HOST') == 'localhost'
    assert config.get('API_PORT') == 8080