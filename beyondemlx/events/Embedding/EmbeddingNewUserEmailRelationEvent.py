from beyondemlx.emlx.EmlxMail import EmlxMail
from beyondemlx.emlx.parts.Participant import Participant
from beyondemlx.events.Embedding.EmbeddingEvent import EmbeddingEvent


class EmbeddingNewUserEmailRelationEvent(EmbeddingEvent):

    def __init__(self, user: Participant, email: EmlxMail, relation: str):
        self.user = user
        self.email = email
        self.relation = relation

    def get_user(self) -> Participant:
        """
        Returns the user.
        """
        return self.user

    def get_email(self) -> EmlxMail:
        """
        Returns the domain.
        """
        return self.email

    def get_relation(self) -> str:
        """
        Returns the relation.
        """
        return self.relation