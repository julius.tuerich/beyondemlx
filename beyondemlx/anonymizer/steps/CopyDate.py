import re

from beyondemlx.anonymizer.steps.Step import Step
from beyondemlx.emlx.EmlxMail import EmlxMail


class CopyDate(Step):

    def execute(self, mail: EmlxMail, anonymized_mail: EmlxMail) -> EmlxMail:
        """
        Copy the date header of the mail.
        """
        anonymized_mail.add_header('Date', mail.get_header('Date'))

        return mail
